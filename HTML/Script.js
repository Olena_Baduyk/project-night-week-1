var counter = 0;
var string1 = "Nice to meet you user. I've got a project in mind for you to do. I love MadLib documents, and that's what I'd like you to make";
var string2 = "Here's an example for you: </br>I've been on so many blind dates this year, I actually went to dinner with the same {INSERT NOUN} twice!"
var string3 = "Now I'm told to put a NOUN in the appropriate space. For example: bird <br/>";
var string4 = "Our new sentence would be: <br/>";
var string4 = "I've been on so many blind dates this year, I actually went to dinner with the same bird twice!<br/>"
var string5 = "What a LOL-ercoaster";
var string6 = "Ok, so here's what I want. I'd like for you to make a program where a user is shown a sentence/paragraph";
var string7 = "5 of the words should spaces where the user will know that their input will be recieved";
var string8 = "The user should be able to input 5 words, these inputs will then be used to modify the sentence";
var string9 = "The sentence, with the users modified words, will then be output to the html page";

function buttonClicked(){
    counter++;

    if (counter == 1)
    {        
        document.getElementById('ptext').style.color = "black";
        document.getElementById('secret1').style.color = "black";
        document.getElementById('secret2').style.color = "black";
        document.getElementById('secret3').style.color = "black";

        document.getElementById('btn').innerHTML = "Continue";
        document.getElementById('ptext').innerHTML = string1;        
        document.getElementById('secret1').innerHTML = "";
        document.getElementById('secret2').innerHTML = "";
        document.getElementById('secret3').innerHTML = "";
    }
    else if (counter == 2)
    {
        document.getElementById('ptext').innerHTML = string2;
        document.getElementById('secret1').innerHTML = string3;
        document.getElementById('secret2').innerHTML = string4;
        document.getElementById('secret3').innerHTML = string5;
    }
    else if (counter == 3)
    {
        document.getElementById('ptext').innerHTML = string6;
        document.getElementById('secret1').innerHTML = string7;
        document.getElementById('secret2').innerHTML = string8;
        document.getElementById('secret3').innerHTML = string9;
    }
}

var astring1 = "Welcome to the next level of this project <br/> I was thinking about what I asked you, and it's just not doing it";
var astring2 = "What I was thinking is the mad lib sentence/paragraph that you've made should be random each time it's run.";
var astring3 = "So what I'd like you to do is store the a list/array of 50 words. Then, put random words into your sentence/paragraph";
var astring4 = "You'll know that you've done it correctly when each time you create a new sentence the words are different (most of the time) <br/> Good Luck";

function agile1() {
    document.getElementById('ptext').style.color = "blue";
    document.getElementById('secret1').style.color = "blue";
    document.getElementById('secret2').style.color = "blue";
    document.getElementById('secret3').style.color = "blue";

    document.getElementById('ptext').innerHTML = astring1;
    document.getElementById('secret1').innerHTML = astring2;
    document.getElementById('secret2').innerHTML = astring3;
    document.getElementById('secret3').innerHTML = astring4;

    counter = 0;
}

var astring5 = "One final thing, I was thinking this project is still missing something";
var astring6 = "All that I ask is to make a loop so the user can print as many of the MadLibs as they want to";
var astring7 = "The user should be able to choose how many times it wants the loop to run";
var astring8 = "So that they don't get over burdened with MadLibs <br/> Good Luck";

function agile2() {
    document.getElementById('ptext').style.color = "green";
    document.getElementById('secret1').style.color = "green";
    document.getElementById('secret2').style.color = "green";
    document.getElementById('secret3').style.color = "green";

    document.getElementById('ptext').innerHTML = astring5;
    document.getElementById('secret1').innerHTML = astring6;
    document.getElementById('secret2').innerHTML = astring7;
    document.getElementById('secret3').innerHTML = astring8;

    counter = 0;
}